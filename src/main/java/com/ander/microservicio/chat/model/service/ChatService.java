package com.ander.microservicio.chat.model.service;

import java.util.List;

import com.ander.microservicio.chat.model.documents.Mensaje;


public interface ChatService {

	public List<Mensaje> obtenerUltimos10Mensajes();
	public Mensaje guardar(Mensaje mensaje);
}
